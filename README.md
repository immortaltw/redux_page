Inspired by https://github.com/StephenGrider/ReduxSimpleStarter

### Summary
A small React-Redux project allowing user (me) to make a post (published or unpublished) on a page by utilizing Facebook Graph API.
