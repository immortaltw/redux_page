import { GET_POSTS, UPDATE_POST, PUBLISH_POST } from '../actions';
import _ from 'lodash';

export default function(state = null, action) {
    switch(action.type) {
        case GET_POSTS:
            return _.mapKeys(action.payload.data, 'id');
        case UPDATE_POST:
            state[action.payload.id]["is_published"] = true;
            return state;
        case PUBLISH_POST:
            return {...state, [action.payload.id]: action.payload};
        default:
            return state;
    }
}