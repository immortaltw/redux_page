import { LOGIN } from '../actions';

export default function(state = {status: '', authResponse: {}}, action) {
    switch(action.type) {
        case LOGIN:
            return action.payload;
        default:
            return state;
    }
}