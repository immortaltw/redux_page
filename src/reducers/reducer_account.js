import { ACCOUNT, GET_AUTH_TOKEN } from '../actions';
import { FB_PAGE_NAME } from '../config/fb_config';
import _ from 'lodash';

export default function(state = null, action) {
    switch(action.type) {
        case ACCOUNT:
            if (action.payload.hasOwnProperty('error')) {
                return state;
            }
            const page = _.find(action.payload.data, 'name', FB_PAGE_NAME);
            return {...state, "id": page.id};
        case GET_AUTH_TOKEN:
            return {...state, "access_token": action.payload.access_token};
        default:
            return state;
    }
}