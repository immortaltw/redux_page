import { GET_POST_IMPRESSION } from '../actions';

export default function(state = {}, action) {
    switch(action.type) {
        case GET_POST_IMPRESSION:
            const id = action.payload.data[0].id.split('/')[0];
            return {...state, [id]: action.payload.data[0].values[0].value};
        default:
            return state;
    }
}