import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import LoginReducer from './reducer_login';
import AccountReducer from './reducer_account';
import PostsReducer from './reducer_posts';
import InsightsReducer from './reducer_posts_insights';

const rootReducer = combineReducers({
  login: LoginReducer,
  account: AccountReducer,
  posts: PostsReducer,
  insights: InsightsReducer,
  form: formReducer
});

export default rootReducer;
