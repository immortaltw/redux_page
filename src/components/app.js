import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getLoginStatus, getAccountInfo, getPageAuthToken } from '../actions'
import PostList from './post_list';
import PostNew from './post_new';
import Login from './login';
import { FB_INIT_CONFIG, FB_PAGE_NAME } from '../config/fb_config';

class App extends Component {

    // Initialize FB sdk
    componentDidMount() {
        window.fbAsyncInit = () => {
          FB.init(FB_INIT_CONFIG);

          // Get login status and account info.
          this.props.getLoginStatus(() => {
            this.props.getAccountInfo((response) => {
              if (response.data) {
                const page = _.find(response.data, 'name', FB_PAGE_NAME);
                this.props.getPageAuthToken(page.id);
              }
            });
          });
        }

        (function (d, s, id) {
          const fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) { return; }
            const js = d.createElement(s); js.id = id;
            js.src = '//connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    render() {
        // Waiting for login status...
        if (!this.props.login.status) {
            return <div>Loading...</div>;
        }

        // Not connected, show login button
        if (this.props.login.status!=="connected") {
            return <div><Login /></div>;
        }

        return (
          <div>
              <PostList />
              <PostNew />
          </div>
        );
    }
}

function mapStateToProps({ login, account }) {
  return { login, account };
}

export default connect(mapStateToProps, { getLoginStatus, getAccountInfo, getPageAuthToken })(App);