import React, { Component } from 'react';
import { connect } from 'react-redux';
import PostItem from './post_item';
import { getPosts } from '../actions'
import { FB_PAGE_NAME } from '../config/fb_config';
import _ from 'lodash';

class PostList extends Component {

    renderPostList() {
        return _.map(this.props.posts, (post) => <PostItem key={post.id} post={post} />);
    }

    render() {
        if (!this.props.account) {
            return <div>Loading...</div>;
        }

        if (!this.props.posts) {
            this.props.getPosts(this.props.account.id);
            return <div>Loading...</div>;
        }

        return (
            <div className="col-md-6 posts-col">
                <h3>Posts</h3>
                <ul className="list-group">
                    {this.renderPostList()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps({ account, posts }) {
  return { account, posts }; 
}

export default connect(mapStateToProps, { getPosts })(PostList);