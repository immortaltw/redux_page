import React, { Component } from 'react';
import { connect } from 'react-redux';
import { doLogin, getAccountInfo, getPageAuthToken } from '../actions';
import { FB_PAGE_NAME } from '../config/fb_config';

class Login extends Component {
    render() {
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">

                <div className="navbar-header">
                    <p className="navbar-text">Login First</p>
                </div>
                <button
                    className='btn btn-primary navbar-btn'
                    onClick={() => this.props.doLogin(() => {
                        this.props.getAccountInfo((response) => {
                            const page = _.find(response.data, 'name', FB_PAGE_NAME);
                            this.props.getPageAuthToken(page.id);
                        });
                    })}>
                    Login
                </button>
                </div>
            </nav>
        );
    }
}

function mapStateToProps(state) {
    return state;
}

export default connect(mapStateToProps, {doLogin, getAccountInfo, getPageAuthToken})(Login);