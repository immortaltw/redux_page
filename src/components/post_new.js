import React, { Component } from 'react';
import { Field, reduxForm, reset } from 'redux-form';
import { connect } from 'react-redux';
import { publishPost } from '../actions'
import { FB_PAGE_NAME } from '../config/fb_config';

class PostNew extends Component {
    renderInputField(field) {
        const { meta: { touched, error } } = field;
        return (
            <div className="form-group">
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input}
                />
                <p className="text-danger">
                    {touched? error: ""}
                </p>
            </div>
        );
    }

    onSubmit(values) {
        console.log(values);
        if (!values.content || !values.attribute) return;

        const fields = {"message": values.content,
                        "published": values.attribute==="publish"? true: false,
                        "access_token": this.props.account.access_token,
                        "fields": "created_time,message,is_published"
                        }
        this.props.publishPost(this.props.account.id, fields);
    }

    render() {
        if (!this.props.posts) return null;

        const { handleSubmit } = this.props;
        return (
            <div className="col-md-6 posts-col">
                <h3>Create New Post</h3>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <Field
                        label="content"
                        name="content"
                        component={this.renderInputField}
                    />
                    <div>
                        <label className="radio-inline">
                            <Field name="attribute"
                                component="input"
                                type="radio"
                                value="publish"
                            />
                            publish
                        </label>
                        <label className="radio-inline">
                            <Field name="attribute"
                                component="input"
                                type="radio"
                                value="unpublish"
                            />
                            unpublish
                        </label>
                    </div>
                    <button type="submit" className="btn btn-primary">Post</button>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};
    if (!values.content) {
        errors.content = "enter something";
    }
    if (!values.attribute) {
        errors.attribute = "select an attribute";
    }
    return errors;
};

const afterSubmit = (result, dispatch) =>
    dispatch(reset('PublishPostForm'));

function mapStateToProps({ account, posts }) {
  return { account, posts };
}

export default reduxForm({
  validate,
  form: 'PublishPostForm',
  onSubmitSuccess: afterSubmit
})(
  connect(mapStateToProps,{ publishPost })(PostNew)
);