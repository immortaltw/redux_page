import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updatePost, getPostImpression } from '../actions'
import { FB_PAGE_NAME } from '../config/fb_config';

class PostItem extends Component {
    componentDidMount() {
        this.props.getPostImpression(this.props.post.id);
    }

    onPublishButtonClicked() {
        const fields = {
            "is_published": true,
            "access_token": this.props.account.access_token,
            "fields": "is_published"
        }
        this.props.updatePost(this.props.post.id, fields);
    }

    renderPublishButton() {
        if (!this.props.post.is_published) {
            return <button className='btn btn-danger btn-xs' onClick={this.onPublishButtonClicked.bind(this)}>Publish</button>
        }
        return null;
    }

    render() {
        console.log(this.props.insights)
        const bgClass = this.props.post.is_published? "label label-primary": "label label-danger";
        return (
            <li className='list-group-item'>
                <div>
                    <span>ID: {this.props.post.id}</span>
                    <p>{this.props.post.message}</p>
                    <p>views: {!this.props.insights? 0: this.props.insights[this.props.post.id]}</p>
                    <span className={bgClass}>status: {this.props.post.is_published? "published": "unpublished"}</span>
                    {/*<div className="text-xs-right">{this.renderPublishButton()}</div>*/}
                </div>
            </li>
        );
    }
}

function mapStateToProps({ account, posts, insights }) {
  return { account, posts, insights }; 
}

export default connect(mapStateToProps, { updatePost, getPostImpression })(PostItem);