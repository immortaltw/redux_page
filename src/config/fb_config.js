export const FB_INIT_CONFIG = {
            appId: '324462021304289',
            cookie: true,
            xfbml: false,
            version: 'v2.9'
        };

export const FB_LOGIN_CONFIG = {
            scope: 'manage_pages,publish_pages,read_insights',
            return_scopes: true
        };

export const FB_PAGE_NAME = "Immortaltw Inc.";