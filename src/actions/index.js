import FB_LOGIN_CONFIG from '../config/fb_config'

export const LOGIN = "login";
export const ACCOUNT = "account";
export const GET_AUTH_TOKEN = "get_auth_token";
export const GET_POSTS = "get_posts";
export const UPDATE_POST = "update_post";
export const PUBLISH_POST = "publish_post";
export const GET_POST_IMPRESSION = "get_post_impression";

// Get FB login status.
export function getLoginStatus(cb) {
    if (!window.FB) {
        return {
            type: LOGIN,
            payload: { status: '', authResponse: {} }
        };
    }

    const request = new Promise((resolve, reject) => {
        window.FB.getLoginStatus((response) => {
            resolve(response);
        });
    });

    if (cb)
        request.then(() => cb());

    return {
        type: LOGIN,
        payload: request
    };
}

// Perform FB login.
export function doLogin(cb) {
    if (!window.FB) {
        return {
            type: LOGIN,
            payload: { status: '', authResponse: {} }
        };
    }

    const request = new Promise((resolve, reject) => {
        window.FB.login((response) => {
            resolve(response);
        }, {
            scope: 'manage_pages,publish_pages,read_insights',
            return_scopes: true
        });
    });

    if (cb)
        request.then(() => cb());

    return {
        type: LOGIN,
        payload: request
    };
}

// Get account info.
export function getAccountInfo(cb) {
    if (!window.FB) {
        return {
            type: ACCOUNT,
            payload: {}
        };
    }
    const uri = '/me/accounts';
    const request = new Promise((resolve, reject) => {
        window.FB.api(uri, {}, (response) => {
            resolve(response);
        });
    });

    if (cb) request.then((response) => cb(response));

    return {
        type: ACCOUNT,
        payload: request
    };
}

// Get page auth token.
export function getPageAuthToken(id) {
    if (!window.FB) {
        return {
            type: GET_AUTH_TOKEN,
            payload: {}
        };
    }
    const uri = `/${id}?fields=access_token`;
    const request = new Promise((resolve, reject) => {
        window.FB.api(uri, {}, (response) => {
            resolve(response);
        });
    });

    return {
        type: GET_AUTH_TOKEN,
        payload: request
    };
}

// Get posts.
export function getPosts(id) {
    if (!window.FB) {
        return {
            type: GET_POSTS,
            payload: {}
        };
    }

    const uri = `/${id}/promotable_posts`;
    const request = new Promise((resolve, reject) => {
        window.FB.api(uri, {fields: 'message,is_published,created_time'}, (response) => {
            resolve(response);
        });
    });

    return {
        type: GET_POSTS,
        payload: request
    };
}

// Update post.
export function updatePost(id, fields, cb) {
    if (!window.FB) {
        return {
            type: UPDATE_POST,
            payload: {}
        };
    }
    const uri = `/${id}`;
    const request = new Promise((resolve, reject) => {
        window.FB.api(uri,"POST", fields, (response) => {
            resolve(response);
        });
    });

    if (cb) request.then((response) => cb(response));

    return {
        type: UPDATE_POST,
        payload: request
    };
}

// Publish post.
export function publishPost(id, fields) {
    if (!window.FB) {
        return {
            type: PUBLISH_POST,
            payload: {}
        };
    }

    const uri = `/${id}/feed`;
    console.log(uri);
    console.log(fields);
    const request = new Promise((resolve, reject) => {
        window.FB.api(uri,"POST", fields, (response) => {
            resolve(response);
        });
    });

    return {
        type: PUBLISH_POST,
        payload: request
    };
}

// Get post impression.
export function getPostImpression(id) {
    if (!window.FB) {
        return {
            type: GET_POST_IMPRESSION,
            payload: {}
        };
    }

    const uri = `/${id}/insights/post_impressions_unique`;
    const request = new Promise((resolve, reject) => {
        window.FB.api(uri, {}, (response) => {
            resolve(response);
        });
    });

    return {
        type: GET_POST_IMPRESSION,
        payload: request
    };
}